# PDF Flake

## Information
THis is just a simple nix flake that installs ghostscript into a dev shell for working with pdfs via the command line.
If you come accross this, feel free to use it.

## Useage
Along with the nix flake that adds ghostscript and imageick to a dev shell for you, I also have a script that will run to commands for you. At the moment, this script can only deal with .jpg, .png & .pdf files directly, if you require a different image type to be handled by the script, feel free to fork and create a merge request. Commands to run:
``nix develop`` - Creats the devshell to add the programs to the environment
``./automate.sh`` - Runs the script provided there is an in/ folder in this directory, don't worry it is in the .gitignore so you don't accidentally commit things you dont want committed
