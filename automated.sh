#!/usr/bin/env bash

# Cleaning the environment from the last run of the script
rm -rf middle || echo "Already clean directory"
rm -rf out || echo "Already clean directory"

# Making the folders required for the combination and conversion
mkdir middle
mkdir out

# Dealing with images, will deal with more of them as required
for f in "in/"*.jpg; do
	if [ ${f} == "in/*.jpg" ]; then
		break
	fi
	filename=$(basename ${f})
	convert "${f}" -page a4 "./middle/${filename%.*}.pdf"
done

for f in "in/"*.png; do
	if [ ${f} == "in/*.png" ]; then
		break
	fi
	filename=$(basename ${f})
	convert "${f}" -page a4 "./middle/${filename%.*}.pdf"
done

# Deal with pdfs that are pdfs from the start
for f in "in/"*.pdf; do
	if [ ${f} == "in/*.pdf" ]; then
		break
	fi
	cp ${f} ./middle/
done

# Get the user to input a file name
echo "Please input a file name for the combined pdf, leaving out the file extension: (Default: out.pdf"
read name
if [ $name -z ]; then
	name="out"
fi

# Convert to one pdf
gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=./out/${name}.pdf -dBATCH "middle/"*.pdf
